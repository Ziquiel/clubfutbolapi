package com.captton.clubfutbolapiforeal.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.captton.clubfutbolapiforeal.model.Club;
import com.captton.clubfutbolapiforeal.model.InputClub;
import com.captton.clubfutbolapiforeal.model.InputJugador;
import com.captton.clubfutbolapiforeal.model.Jugador;
import com.captton.clubfutbolapiforeal.repository.ClubDao;
import com.captton.clubfutbolapiforeal.repository.JugadorDao;



@RestController
@RequestMapping({ "/clubfutbol" })
public class Controller {
	@Autowired
	ClubDao clubDao;
	@Autowired
	JugadorDao jugDao;

//--------------------jugador------------------------------------
	@CrossOrigin(origins = "http://localhost:4200")// da de alta con afa a un jugador
	@RequestMapping({ "/alta" })
	@PostMapping
	public ResponseEntity<Object> alta(@RequestBody InputJugador dao) {
		Club club = clubDao.findByName("AFA");
		Jugador jug = new Jugador();
		if (club != null) {
			jug.setClub(club);
			jug.setName(dao.getName());
			jug.setAge(dao.getAge());
			jug.setImg(dao.getImg());
			Jugador jugador = jugDao.save(jug);
			JSONObject obj = new JSONObject();
			obj.put("error", 0);
			obj.put("message", jugador.getId());
			return ResponseEntity.ok().body(obj.toString());

		} else {
			JSONObject obj = new JSONObject();
			obj.put("error", 1);
			obj.put("message", "club not found");
			return ResponseEntity.ok().body(obj.toString());
		}
	}
	@CrossOrigin(origins = "http://localhost:4200") //setea club
	@RequestMapping({ "/fichar/{idc}/{idj}" })
	@PutMapping
	public ResponseEntity<Jugador> fichar (@PathVariable("idc") Long idc,@PathVariable("idj") Long idj ) {
		Club club = clubDao.findById(idc).orElse(null);
		return jugDao.findById(idj).map(record->{
			record.setClub(club);
			Jugador updated = jugDao.save(record);
			return ResponseEntity.ok().body(updated);
			}).orElse(ResponseEntity.notFound().build());
			
	}
	
//	@CrossOrigin(origins = "http://localhost:4200")
//	@RequestMapping({ "/jugador/altasinclub" })
//	@PostMapping
//	public ResponseEntity<Object> altasin(@RequestBody InputJugador dao) {
//		Jugador jug = new Jugador();
//		jug.setName(dao.getName());
//		jug.setAge(dao.getAge());
//		jug.setImg(dao.getImg());
//		Jugador jugador = jugDao.save(jug);
//		JSONObject obj = new JSONObject();
//		obj.put("error", 0);
//		obj.put("message", jugador.getId());
//		return ResponseEntity.ok().body(obj.toString());
//	}

//-----------------club--------------------------------------------
	@CrossOrigin(origins = "http://localhost:4200")// da de alta un club
	@RequestMapping({ "/club/alta" })
	@PostMapping
	public ResponseEntity<Object> alta(@RequestBody InputClub dao) {
		Club club = new Club();
		club.setName(dao.getName());
		club.setLogo(dao.getLogo());
		club.setPassword(dao.getPassword());
		Club club1 = clubDao.save(club);
		JSONObject obj = new JSONObject();
		obj.put("error", 0);
		obj.put("message", club1.getId());
		return ResponseEntity.ok().body(obj.toString());
	}
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping(path = { "/club/{id}" })// obtiene jugador por id de club (todos)
	public ResponseEntity<Object> getListajugadores(@PathVariable Long id) {
		Club club = clubDao.findById(id).orElse(null);
		List<Jugador> jugadoresFound = club.jugadores;
		JSONArray json_array = new JSONArray();

		if (jugadoresFound.size() > 0) {
			for (Jugador jug : jugadoresFound) {
				JSONObject aux = new JSONObject();
				aux.put("ID: ", jug.getId());
				aux.put("Nombre: ", jug.getName());
				aux.put("Edad: ", jug.getAge());
				aux.put("Imagen: ", jug.getImg());
				json_array.put(aux);
			}
			JSONObject obj = new JSONObject();
			obj.put("error", 0);
			obj.put("results", json_array);
			return ResponseEntity.ok().body(obj.toString());

		} else {
			JSONObject obj = new JSONObject();
			obj.put("error", 1);
			obj.put("results", "Este Club no posee jugadores");
			return ResponseEntity.ok().body(obj.toString());
		}

	}
	
	

	}


