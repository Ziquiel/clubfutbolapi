package com.captton.clubfutbolapiforeal.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.clubfutbolapiforeal.model.Club;

public interface ClubDao extends JpaRepository<Club, Long> {
	public Club findByName(String name);
}
