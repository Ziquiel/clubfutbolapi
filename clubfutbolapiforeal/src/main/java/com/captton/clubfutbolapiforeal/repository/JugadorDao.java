package com.captton.clubfutbolapiforeal.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.captton.clubfutbolapiforeal.model.Jugador;

public interface JugadorDao extends JpaRepository<Jugador, Long> {

}
