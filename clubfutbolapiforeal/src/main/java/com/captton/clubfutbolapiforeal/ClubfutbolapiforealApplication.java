package com.captton.clubfutbolapiforeal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClubfutbolapiforealApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClubfutbolapiforealApplication.class, args);
	}

}
